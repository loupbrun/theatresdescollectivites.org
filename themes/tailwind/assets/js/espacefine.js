/**
 * Effectue un rechercher-remplacer pour ajouter des espaces fines avant les
 * signes doubles et à l'intérieur des guillemets.
 *
 * @see Basé sur un code de rechercher-remplacer par James Padolsey
 *      http://james.padolsey.com/javascript/find-and-replace-text-with-javascript/
 * @author Victor <hey@victorloux.fr>
 * @license http://www.wtfpl.net/txt/copying/ WTFPL version 2
 * @param  {node} searchNode Un élément HTML, ou une NodeList
*                            (p.ex. document.getElementsByClassName('fine'))
 * @return {void}
 */

function espaceFine(searchNode){var regex=new RegExp(/(\u00AB|\u2014)(?:\s+)?|(?:\s+)?([\?!:;\u00BB])/g),espace='<span style="font-size: 0.67em">&nbsp;</span>',balisesExclues='html,head,style,title,link,meta,script,object,iframe,pre,code,textarea,noscript';if(!searchNode){searchNode=document.body;}var _nodes,_nodeActuel,_parent,_fragment,_isNodeList=(typeof searchNode==='object'&&/^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(searchNode))&&(Object.prototype.hasOwnProperty.call(searchNode,'length'))&&(searchNode.length===0||(typeof searchNode[0]==="object"&&searchNode[0].nodeType>0))),_nodes=_isNodeList?searchNode:searchNode.childNodes,_nodeIndex=_nodes.length,_balisesExclues=balisesExclues.split(',');while(_nodeIndex--){_nodeActuel=_nodes[_nodeIndex];if(_nodeActuel==document.body){continue}if(_nodeActuel.nodeType===1&&_balisesExclues.indexOf(_nodeActuel.nodeName.toLowerCase())===-1){espaceFine(_nodeActuel)}if(_nodeActuel.nodeType!==3||!regex.test(_nodeActuel.data)){continue}_parent=_nodeActuel.parentNode;_fragment=(function(){var html=_nodeActuel.data.replace(regex,"$1"+espace+"$2"),wrap=document.createElement('div'),frag=document.createDocumentFragment();wrap.innerHTML=html;while(wrap.firstChild){frag.appendChild(wrap.firstChild)}return frag})();_parent.insertBefore(_fragment,_nodeActuel);_parent.removeChild(_nodeActuel)}}
