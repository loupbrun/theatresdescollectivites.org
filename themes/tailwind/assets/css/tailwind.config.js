module.exports = {
  theme: {
    fontFamily: {
      display: ['Double', 'serif'],
      body: ['Ilisarniq', 'sans-serif']
    },
    extend: {
      colors: {
        beige: '#e5d8bf',
        green: '#94aa2a',
        yellow: '#e4b012',
        indigo: '#494df5',
        orange: '#e47312',
        red: '#d55252',
        lightblue: '#8daaea',
        darkturquoise: '#113e4f',
      },
      fontSize: {
        '7xl': '5rem', 
        '8xl': '6rem', 
      },
      screens: {
        'xxl': '1600px'
      },
    }
  },
  variants: {},
  plugins: []
}
