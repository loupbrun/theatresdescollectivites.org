---
title: Partenaires
description: "Le Colloque Théâtres des collectivités est organisé par la Société québécoise d’études théâtrales (SQET) en collaboration avec…"
---

Le Colloque Théâtres des collectivités est organisé par la Société québécoise d’études théâtrales (SQET), en collaboration avec :

- Le Centre de recherche interuniversitaire sur la littérature et la culture
québécoises ([CRILCQ](http://www.crilcq.org/accueil/))
- Le Centre de recherches intermédiales sur les arts, les lettres et les techniques ([CRIALT](https://crialt-intermediality.org/fr/pages1/))
- Le Groupe international [Arts trompeurs](http://www.lesartstrompeurs.labex-arts-h2h.fr/fr)
- Le Centre de recherche interuniversitaire sur les humanités numériques ([CRIHN](https://www.crihn.org/))