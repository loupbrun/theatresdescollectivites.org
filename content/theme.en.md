---
title: Theme for the International Symposium

---

**“The Theatres of Collectives”**

The successive controversies triggered by the 2018 announcement of the *Slàv* and *Kanata* performances, staged by Robert Lepage, brought to the attention of a wide audience subjects that were hitherto confined to a withdrawn milieu of specialists and particular advocacy groups. Who owns this story, who can talk about it, who can define its identity, and under what conditions? Such questions have, with considerable virulence, fuelled an important public debate, to which the press has responded. Very quickly, other corollary and equally crucial questions have emerged, balancing principles of "cultural property" and of creative freedom.

Some lament theatrical projects that, at the outset, were based on overt values of inclusion and restorative will, having been denounced by representatives of (black and first nations) collectives who, in fact, highlighted the painful history. But what is even more unfortunate is that, among the abundance of exchanges brought about by this debate, little attention has been paid to the fact that the theatrical milieu, constituted of those who are active in these communities, have in large part been at the root of the controversy. The *Slàv-Kanata* affair has thereby steered focus towards current affairs and related questions on culture and the history of certain collectives; and yet, the affair has revealed nothing regarding what these particular collectives, as well as others who do not partake in the debate, have been doing for years, sometimes for decades, precisely in order to "tell themselves."

This important and profuse activity, which remains eclipsed by the controversy, is at the heart of this international symposium on "The Theatres of Collectives." It is devoted to this "other" theatrical voice which, plural and diverse in its form and its content, is the result of community initiatives, regardless of the situation, nature, language and activism of the communities concerned. While disparate, these collectives nonetheless share the desire to express, to recover and to recognize themselves in stage practices where no one individual has an overall vision. Nevertheless, statistics available in North America and in Europe demonstrate that this theatre of collectives draws more spectators and more practitioners than the so-called commercial and traditionally professional productions, that it establishes an entirely original stage-audience relationship and that it creates a spectacular and social experience beyond compare.

Overly thought about and under-documented, this theatre plays an active role in the development and assertion of these collectives. It does this by taking extremely diverse forms, by adopting modes of production and of creation that resonate with its milieu and by pursuing original objectives.

The expression "theatre of collectives" covers a wide range of stage practises, enabling consideration of their diversity but also of their hybridity and their evolving energy[^1]. It also avoids confinement to common yet unsuitable categories, such as amateurs, school or professional---theatrical groups passing from amateur to professional status and vice-versa, others hiring professionals, sometimes on a regular or provisional basis, others lastly based on a stable core of paid agents, etc.

The symposium offers the opportunity for practitioners, theorists, critics, historians and administrators to discuss the topics of "theatres of collectives" based on five avenues of reflection:

-   Historical, epistemological and theoretical perspectives

-   Aesthetics objectives, innovative practices

-   Militant engagement: social, identity-based, political demands; actions for and from the milieu

-   Organizational challenges and their modes of functioning

-   Practices of reception: how should we see and critique the theatre of collectives?

[^1]: For example, first nations theatre, those of ethno-cultural minorities, theatre for those living with disabilities, theatre from LGBTQ+ communities, regional and local theatrical groups, theatre in school groups, institutional theatre (in prisons, etc.) and business theatre, etc.
