---
title: Programme
---

Download the [**programme**](/documents/Prog-ThCollec2022.06.14.pdf) (PDF, 2.8 Mb)  
<small>* Last updated: June 14</small>

Download the [**long-length version**](/documents/Prog-long-thColl2022.06.14.pdf) (PDF, 5.9 Mb)  
<small>* Last updated: June 14</small>

## Online Links

The symposium will be available online via Zoom (the links will remain the same for all three days):

- Zoom link to [**room C-3061**](https://umontreal.zoom.us/j/86785805412?pwd=WGNJQnZ5SXNFdU1tdkh2QnpyWlRZdz09)
- Zoom link to [**room C-1017-02**](https://umontreal.zoom.us/j/81750306942?pwd=bFYzVnM2NThtSDVrY0ZyY0FGaHd6UT09)

Only the shows and performances will not be broadcast online, including events in C-1070 or at the Centre d’essai.
