---
title: Programme
---

Télécharger le [**programme**](/documents/Prog-ThCollec2022.06.14.pdf) (PDF, 2,8 Mo)  
<small>* Dernière mise à jour le 14 juin 2022</small>

Télécharger le [**programme long**](/documents/Prog-long-thColl2022.06.14.pdf) (PDF, 5,9 Mo)  
<small>* Dernière mise à jour le 14 juin 2022</small>

## Diffusion en ligne

Le colloque sera diffusé en continu via Zoom (ce sont les mêmes liens pour tous les jours) :

- Lien Zoom vers la [**salle C-3061**](https://umontreal.zoom.us/j/86785805412?pwd=WGNJQnZ5SXNFdU1tdkh2QnpyWlRZdz09)
- Lien Zoom vers la [**salle C-1017-02**](https://umontreal.zoom.us/j/81750306942?pwd=bFYzVnM2NThtSDVrY0ZyY0FGaHd6UT09)

Les seules séances et activités qui ne seront pas en co-modal sont les spectacles et les performances, notamment tout ce qui se déroule au C-1070 ou au Centre d'essai.
