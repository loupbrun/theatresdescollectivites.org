---
title: Meals
description: "Meals at noon will be served on site. The banquet will be held on the evening of May 27."
---

## For invited speakers

A cold buffet will be served on site every day at noon on the days of the colloquium for the invited speakers only.

## Nearby Locations

**Première Moisson**: bakery, pastry & café   
https://premieremoisson.com

5199 chemin de la Côte-des-Neiges, Montréal (QC); H3T 1Y1

**Au pain doré**: bakery, pastry & café  

5214 Chemin de la Côte-des-Neiges, Montréal, (QC); H3T 1Y1  
http://fr.aupaindore.com/

**Resto-bar La Maisonnée**  
5385 Avenue Gatineau, Montréal (QC); H3T 1X2  
http://restobarlamaisonnee.com/

**Pub McCarold**: Irish Pub  
5400 Chemin de la Côte-des-Neiges, Mtl (QC) ; H3T 1Y5  
http://pubmccarold.com/

**Brûlerie urbaine**: coffee roaster  
5252 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1X8

**Saint-Houblon** : brasserie  
5414 rue Gatineau, Montréal (QC); H3T 1X5  
https://www.sainthoublon.com/

**Kinton Ramen**  
5216 Chemin de la Cote-des-Neiges, Montréal (QC) H3T 1X8  
https://www.kintonramen.com/quebec-menu/#ramen-qc

## Groceries and Alcohol

**Exofruits**  
5192 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1Y1

**Épicerie Métro**  
5150 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1X8

**Société des Alcools du Québec (SAQ)**  
5252 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1X8

## Suggestions of restaurants, snack bars and cafes

**Aux Vivres**: vegan  
4631 Boul. St-Laurent, Montréal (QC); H2T 1R2  
Price (evening): approximately **$45** for 2 people before taxes and service  
https://www.auxvivres.com/notre-menu

**Au petit extra**  
1690 Rue Ontario E, Montréal (QC); H2L 1S7  
Price (evening): approximately **$80** for 2 people before taxes and service  
http://aupetitextra.com/

**Bouillon Bilk**  
1595 Boul. St-Laurent, Montréal (QC); H2X 2S9  
Price (evening): approximately **$120** for 2 people before taxes and service  
http://bouillonbilk.com/

**Bistrot La Fabrique**  
3609 Rue Saint-Denis, Montréal (QC); H2X 3L6  
Price (evening): approximately **$100** for 2 people before taxes and service  
http://bistrotlafabrique.com/fr/index.html

**Au pied de cochon**  
536 Avenue Duluth E, Montréal (QC); H2L 1A9  
Price (evening): approximately **$100** for 2 people before taxes and service  
http://aupieddecochon.ca/

**Schwartz’s**: smoked meat  
3895 boul. Saint-Laurent, Montréal (QC); H2W 1X9  
Price: approximately **$30** for 2 people before taxes and service  
https://schwartzsdeli.com/index.php?route=common/home

**Leméac**  
1045 Avenue Laurier O, Outremont (QC); H2V 2L1  
Price (evening): approximately **$110** for 2 people before taxes and service  
http://restaurantlemeac.com/

**Joe Beef**  
2491 Rue Notre-Dame Ouest, Montréal (QC); H3J 1N6  
Price (evening): approximately **$130** for 2 people before taxes and service  
http://www.joebeef.ca/fr/

**Le Mousso**  
1023 Rue Ontario E, Montréal (QC); H2L 1P8  
Price (evening): approximately **240$** for 2 people before taxes and service  
https://lemousso.com/

**Restaurant Toqué!**  
Price (evening): approximately **$200** for 2 people before taxes and service  
900 Place Jean-Paul-Riopelle, Montréal (QC); H2Z 2B2  
http://www.restaurant-toque.com/

**Buvette chez Simone**: wine bar  
4869 Av du Parc, Montréal (QC); H2V 4E7  
http://buvettechezsimone.com/

**Café Olimpico**  
124 Rue Saint Viateur O, Montréal (QC); H2T 2L1  
http://www.cafeolimpico.com/

**St-Viateur Bagel Shop**  
263 Rue Saint Viateur O, Montréal (QC); H2V 1Y1

**La Banquise**: poutine, open 24/24h  
994 Rue Rachel E, Montréal (QC); H2J 2J3  
Price (evening): approximately **$25** for 2 people before taxes and service  
http://labanquise.com/
