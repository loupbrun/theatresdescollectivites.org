---
title: Home

# L’avertissement défilera en haut de la page
#avertissement: "**NEW DATES – Due to the current health situation, the new dates for the colloqium are: June 16-19 2022.**"

---

**New dates: June 16-19, 2022**

Place: Montréal, University of Montréal

Organized by:

- The Centre de recherche interuniversitaire sur la littérature et la culture québécoises (CRILCQ)  
- The International University Theater Association (AITU-IUTA)
- The Fédération québécoise du théâtre amateur (FQTA)
- Société québécoise d’études théâtrales (SQET)
