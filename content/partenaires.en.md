---
title: Partners
description: "The Colloquium Théâtres des collectivités is organized by the Société québécoise d’études théâtrales (SQET) in collaboration with…"
---

The Colloquium Théâtres des collectivités is organized by the Société québécoise d’études théâtrales (SQET) in collaboration with:

- The Centre de recherche interuniversitaire sur la littérature et la culture
québécoises ([CRILCQ](http://www.crilcq.org/accueil/))
- The Centre de recherches intermédiales sur les arts, les lettres et les techniques ([CRIALT](https://crialt-intermediality.org/fr/pages1/))
- The Groupe international [Arts trompeurs](http://www.lesartstrompeurs.labex-arts-h2h.fr/fr)
- The Centre de recherche interuniversitaire sur les humanités numériques ([CRIHN](https://www.crihn.org/))
