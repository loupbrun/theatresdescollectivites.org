---
title: Comité organisateur
description: "Voici les membres du comité organisateur pour l’édition 2020 du colloque Théâtres des collectivités."
---

## Organisation générale

- **Erin Hurley** (CRILCQ, Université McGill)
- [**Jean-Marc Larrue**](https://crilcq.org/qui-sommes-nous/membres/jean-marc-larrue/) (CRILCQ, Université de Montréal)
- **Nicole Nolette** (CRILCQ, SQET, University of Waterloo)
- **Dominic Poulin** (Service aux étudiants, Théâtre Université de Montréal)
- **Yoland Roy** (Fédération québécoise du théâtre amateur)
- **Marie-Eve Skelling-Desmeules** (CRILCQ, SQET, Université du Québec à Chicoutimi)
- **Karolann St-Amand** (CRILCQ, Figura, Université de Montréal)

## Organisation logistique

- Coordination et informations générales&nbsp;: **Karolann St-Amand** (CRILCQ, Figura, Université de Montréal)
- Transport et hébergement&nbsp;: **Sarah Grenier** (CRILCQ, Université de Montréal) et Ève-Catherine Champoux (CRILCQ, Université de Montréal)
- Informations scientifiques&nbsp;: **Astrid Novat** (CRILCQ, Université de Montréal)
- Soutien technique&nbsp;: **Louis-Olivier Brassard**
