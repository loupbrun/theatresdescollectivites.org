---
title: Organizing Committee
description: "Members of the organizing committee for the 2020 edition of the Théâtres des collectivités colloquium."
---

## General organization

- **Erin Hurley** (CRILCQ, McGill University)
- [**Jean-Marc Larrue**](https://crilcq.org/qui-sommes-nous/membres/jean-marc-larrue/) (CRILCQ, Université de Montréal)
- **Nicole Nolette** (CRILCQ, SQET, University of Waterloo)
- **Dominic Poulin** (Service aux étudiants, Théâtre Université de Montréal)
- **Yoland Roy** (Fédération québécoise du théâtre amateur)
- **Marie-Eve Skelling-Desmeules** (CRILCQ, SQET, Université du Québec à Chicoutimi)
- **Karolann St-Amand** (CRILCQ, Figura, Université de Montréal)

## Logistics

- Coordination and general information: **Karolann St-Amand** (CRILCQ, Figura, Université de Montréal)
- Lodging and transport: **Sarah Grenier** (CRILCQ, Université de Montréal) et Ève-Catherine Champoux (CRILCQ, Université de Montréal)
- Scientific information: **Astrid Novat** (CRILCQ, Université de Montréal)
- Technical support: **Louis-Olivier Brassard**
