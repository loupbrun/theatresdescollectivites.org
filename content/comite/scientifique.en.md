---
title: Scientific Committee
---

- **Alain Chevalier** (Université of Liège, Belgium)
- **Elka Fedjuk** (Universidad Veracruzana, Mexico)
- **Kathryn Mederos Syssoyeva** (Dixie State University, USA)
- **Vito Minoia** (AENIGMA, Urbino University, Italy)
- **Françoise Odin** (INSA-Lyon, France)
- **Rénald Pelletier** (Fédération québécoise du théâtre amateur)
- **Maria S. Horne** (University at Buffalo – SUNY)
- **Robin Whittaker** (St. Thomas University, Canada)
