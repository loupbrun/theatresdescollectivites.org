---
title: Comité scientifique
---

- **Alain Chevalier** (Université de Liège, Belgique)
- **Elka Fedjuk** (Universidad Veracruzana, Mexique)
- **Kathryn Mederos Syssoyeva** (Dixie State University, USA)
- **Vito Minoia** (AENIGMA, Université d’Urbino, Italie)
- **Françoise Odin** (INSA-Lyon, France)
- **Rénald Pelletier** (Fédération québécoise du théâtre amateur)
- **Maria S. Horne** (University at Buffalo – SUNY)
- **Robin Whittaker** (Université St. Thomas, Canada)
