---
title: Repas
description: "Les repas de midi seront pris sur place. Le banquet est prévu le 27 mai au soir."
---

## Pour les personnes inscrites

Un buffet froid sera servi sur place à midi les jours de colloque pour les invités seulement. 

## À proximité

**Première Moisson** : boulangerie, pâtisserie & café   
https://premieremoisson.com  
5199 chemin de la Côte-des-Neiges, Montréal (QC); H3T 1Y1

**Au pain doré** : boulangerie, pâtisserie & café  
5214 Chemin de la Côte-des-Neiges, Montréal, (QC); H3T 1Y1  
http://fr.aupaindore.com/

**Resto-bar La Maisonnée**  
5385 Avenue Gatineau, Montréal (QC); H3T 1X2  
http://restobarlamaisonnee.com/

**Pub McCarold**  
5400 Chemin de la Côte-des-Neiges, Mtl (QC) ; H3T 1Y5  
http://pubmccarold.com/

**Brûlerie urbaine**  
5252 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1X8

**Saint-Houblon** : brasserie  
5414 rue Gatineau, Montréal (QC); H3T 1X5  
https://www.sainthoublon.com/

**Kinton Ramen**  
5216 Chemin de la Cote-des-Neiges, Montréal (QC) H3T 1X8  
https://www.kintonramen.com/quebec-menu/#ramen-qc


## Épiceries / Société des Alcools

**Exofruits**  
5192 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1Y1

**Épicerie Métro**  
5150 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1X8

**Société des Alcools du Québec (SAQ)**  
5252 Chemin de la Côte-des-Neiges, Montréal (QC); H3T 1X8

## Suggestions de restaurants, casse-croûtes et cafés

**Aux Vivres** : végétalien  
4631 Boul. St-Laurent, Montréal (QC); H2T 1R2  
Prix (soir) : autour de **45 $** pour 2 pers. avant taxes et service  
https://www.auxvivres.com/notre-menu

**Au petit extra**  
1690 Rue Ontario E, Montréal (QC); H2L 1S7  
Prix (soir) : autour de **80 $** pour 2 pers. avant taxes et service  
http://aupetitextra.com/

**Bouillon Bilk**  
1595 Boul. St-Laurent, Montréal (QC); H2X 2S9  
Prix (soir) : autour de **120 $** pour 2 pers. avant taxes et service  
http://bouillonbilk.com/

**Bistrot La Fabrique**  
3609 Rue Saint-Denis, Montréal (QC); H2X 3L6  
Prix (soir) : autour de **100 $** pour 2 pers. avant taxes et service  
http://bistrotlafabrique.com/fr/index.html

**Au pied de cochon**  
536 Avenue Duluth E, Montréal (QC); H2L 1A9  
Prix (soir) : autour de **100 $** pour 2 pers. avant taxes et service  
http://aupieddecochon.ca/

**Schwartz’s** : smoked meat  
3895 boul. Saint-Laurent, Montréal (QC); H2W 1X9  
Prix : autour de **30 $** pour 2 pers. avant taxes et service  
https://schwartzsdeli.com/index.php?route=common/home

**Leméac**  
1045 Avenue Laurier O, Outremont (QC); H2V 2L1  
Prix (soir) : autour de **110 $** pour 2 pers. avant taxes et service  
http://restaurantlemeac.com/

**Joe Beef**  
2491 Rue Notre-Dame Ouest, Montréal (QC); H3J 1N6  
Prix (soir) : autour de **130 $** pour 2 pers. avant taxes et service  
http://www.joebeef.ca/fr/

**Le Mousso**  
1023 Rue Ontario E, Montréal (QC); H2L 1P8  
Prix (soir) : autour de **240$** pour 2 pers. avant taxes et service  
https://lemousso.com/

**Restaurant Toqué!**  
Prix (soir) : autour de **200 $** pour 2 pers. avant taxes et service  
900 Place Jean-Paul-Riopelle, Montréal (QC); H2Z 2B2  
http://www.restaurant-toque.com/

**Buvette chez Simone** : bar à vin  
4869 Av du Parc, Montréal (QC); H2V 4E7  
http://buvettechezsimone.com/

**Café Olimpico**  
124 Rue Saint Viateur O, Montréal (QC); H2T 2L1  
http://www.cafeolimpico.com/

**St-Viateur Bagel Shop**  
263 Rue Saint Viateur O, Montréal (QC); H2V 1Y1

**La Banquise** : poutine, ouvert 24h/24  
994 Rue Rachel E, Montréal (QC); H2J 2J3  
Prix (soir) : autour de **25 $** pour 2 pers. avant taxes et service  
http://labanquise.com/
