---
title: Accueil

# L’avertissement défilera en haut de la page
#avertissement: "**NOUVELLES DATES – En raison de la situation sanitaire actuelle, les nouvelles dates du colloque sont le 16-19 juin 2022**"

---

**Nouvelles dates: 16-19 juin 2022**

Université de Montréal, Montréal, Canada

Organisé par :

- Le Centre de recherche interuniversitaire sur la littérature et la culture québécoises (CRILCQ)  
- L’Association internationale du théâtre à l’Université (AITU-IUTA)
- La Fédération québécoise du théâtre amateur (FQTA)
- Société québécoise d’études théâtrales (SQET)
