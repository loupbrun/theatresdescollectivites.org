---
title: Hébergement
description: "La région de Montréal offre de nombreuses possibilités d’hébergement pour votre séjour lors du colloque. Nous vous proposons une liste d’hôtels."

published: false

---

**Informations post-COVID à venir**

## Studios Hotel – Université de Montréal

Chambre simple : 50$ CAD par nuitée  
Suite : 110$ CAD par nuitée  

Contact : comité organisateur

## Hostel Le jardin d’Antoine

2024, Rue Saint-Denis  
Montreal, Quebec, H2X 2K8  
Téléphone : 514-843-4506  
Sans frais additionnels : 1-800-361-4506  
Fax : 514-281-1491  
Prix : 128$ CAD par nuitée (chambre économique) et 156$ CAD (chambre standard)
Déjeuner inclus

Site web :
https://www.aubergelejardindantoine.com/index.php

Contact à l’hôtel:
reception@aubergelejardindantoine.com

*Cet hôtel offre un tarif préférentiel pour les participants de la conférence.*

## Hotel Château Versailles

1659, Sherbrooke West Street  
Montreal, Quebec, H3H 1E3  
Téléphone : 514-933-3611  
Sans frais additionnels : 888-933-8111  
Fax : 514-933-6967  
Prix : 139$ CAD (double room), 144$ CAD/169$ CAD (king size bed room)  
Déjeuner inclus  

Site web :
https://www.chateauversaillesmontreal.com

*Cet hôtel offre un tarif préférentiel pour les participants de la conférence.*

## Terrasse Royale

5225, Côte-des-Neiges  
Montreal, Quebec, H3T 1Y1  
Téléphone : 514-739-6391  
Fax : 514-342-2512  
Prix : 129$ CAD (studio avec lit king et cuisinette)
Déjeuner non inclus, mais rabais disponible pour les clients de l’hôtel.

Site web :
https://terrasse-royale.com  

Contact à l’hôtel : info@terrasse-royale.com

*Cet hôtel offre un tarif préférentiel pour les participants de la conférence.*

## Hostel Le Pomerol

819 boul. de Maisonneuve Est  
Montréal, Québec, H2L 1Y7  
Téléphone : 514-526-5511  
Sans frais additionnels : 1-800-361-6869  
Fax : 514-523-0143  
Prix : 155$ CAD (double bedroom)  
Déjeuner inclus  

Site web :
https://www.aubergelepomerol.com  

Contact à l’hôtel :
info@aubergelepomerol.com

## Hotel de l’institut

3535 Rue Saint-Denis  
Montreal, Quebec, H2X 3P1  
Téléphone : 438-800-4595  
Sans frais additionnels : 1-855-229-8189 (Canada et USA)
Fax : 514-873-9893  
Prix: 229$ CAD (chambre régulière avec lit king) + 20$ CAD par personne supplémentaire
Déjeuner inclus  

Site web :
http://www.ithq.qc.ca/hotel  

Contact à l’hôtel: 
hotel@ithq.qc.ca

## Hotel Le Relais Lyonnais

1595 Rue Saint-Denis  
Montreal, (Quebec), H2X 3K3  
Téléphone : 514-448-2999  
Fax : 514-448-2991  
Prix : 199$ CAD (chambre double avec lit queen) et 259$ CAD (suite)

Site web :
http://lerelaislyonnais.com

## Le Résidence Inn par Mariott (Montréal Centre-Ville)

2045 Peel Street  
Montreal, (Quebec), H3A 1T6  
Téléphone : 514-935-9224  
Prix : 278$ CAD (studio avec lit queen)   and 283$ CAD (suite avec chambre séparée et salon)
Déjeuner inclus  

Site web :
https://www.marriott.fr/hotels/travel/yulri-residence-inn-montreal-downtown/

## Hotel Ambrose

3422, Stanley Street  
Montreal, Quebec, H3A 1R8  
Téléphone : 514-288-6922  
Fax : 514-288-5757  
Prix : 180$ CAD (chambre double avec lit queen)
Déjeuner inclus pour 10$ CAD supplémentaires  

Site web :
https://www.hotelambrose.ca/fr  

Contact à l’hôtel :
info@hotelambrose.ca

## Novotel Montréal Centre-Ville

1180 rue de la Montagne  
Montreal, Quebec, H3G 1Z1  
Téléphone : 514-861-6000  
Prix : de 219$ CAD à 279$ CAD (selon la date de réservation) (chambre standard avec lit king)
Déjeuner inclus  
Stationnement : 30$ CAD par nuitée  

Site web :
https://all.accor.com/hotel/1151/index.fr.shtml
