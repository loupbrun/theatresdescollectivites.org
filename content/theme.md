---
title: Thème du colloque

---

Thème : **« Les théâtres des collectivités »**

Les controverses successives déclenchées par l'annonce, en 2018, des spectacles *Slàv* et *Kanata,* dont Robert Lepage signait la mise en scène, ont porté à l'attention d'un large public des sujets jusque-là confinés aux mondes restreints des spécialistes et de certains groupes de pression. À qui appartient l'histoire, qui peut la dire, qui peut dire l'identité, à quelles conditions ? Ces questions ont nourri avec une singulière virulence un important débat public auquel la presse a fait un large écho. Très rapidement, d'autres questions corollaires mais tout aussi cruciales se sont imposées, mettant en balance les principes de « propriété culturelle » et de liberté de création.

Certains ont regretté que des projets théâtraux qui, au départ, reposaient sur des valeurs affichées d'inclusion et de volonté réparatrice, aient été dénoncés par des représentant.e.s des collectivités -- noires et autochtones -- dont, précisément, ils soulignaient la douloureuse histoire. Mais ce qu'on peut regretter bien davantage, c'est que, dans l'abondance des échanges qu'a suscités ce débat, on se soit si peu intéressé au fait que ce sont des gens de théâtre, actifs dans ces communautés, qui ont été en bonne partie à l'origine de la controverse. Si l'affaire *Slàv-Kanata* a ainsi permis de braquer les projecteurs de l'actualité sur des questions relatives à la culture et l'histoire de certaines collectivités, elle n'a rien révélé de ce que ces collectivités particulières et d'autres collectivités, qui ne sont pas partie prenante au débat, font depuis des années, parfois des décennies, précisément pour « se dire ».

C'est à cette activité importante et foisonnante, mais restée dans l'ombre de la controverse que le colloque international « Les théâtres des collectivités » est consacré.

Il porte sur cette « autre » parole théâtrale, plurielle et diversifiée dans sa forme et ses contenus, qui est le fruit d'initiatives communautaires, peu importe la situation, la nature, la langue et le militantisme des collectivités concernées. D'une grande disparité, ces collectivités, qu'on retrouve dans de nombreux pays et régions du monde, ont cependant en commun d'exprimer, de se retrouver et se reconnaître dans des pratiques scéniques dont personne n'a de vision d'ensemble. Pourtant, les statistiques disponibles en Amérique du Nord et en Europe montrent que ce théâtre des collectivités attire plus de spectateurs et mobilise plus de praticiens et praticiennes que les scènes dites commerciales et professionnelles traditionnelles, qu'il instaure un rapport scène-salle tout à fait original et qu'il permet une expérience spectaculaire et sociale sans pareil.

Peu pensé et sous documenté, ce théâtre joue un rôle actif dans le développement et l'affirmation de ces collectivités, mais aussi dans l'évolution des pratiques scéniques en général. Il le fait en prenant les formes les plus diverses, en adoptant des modes de production et de création en résonance avec son milieu et en poursuivant des objectifs singuliers mais aussi des objectifs qu'il partage avec d'autres pratiques théâtrales et scéniques.

L'expression « théâtre des collectivités » couvre un large spectre de pratiques scéniques, qu'elle permet de considérer dans toute leur diversité mais aussi dans leur hybridité et leur dynamique évolutive[^1]. Elle évite aussi l'enfermement dans les catégories usuelles mais peu adaptées telles celles d'amateur, scolaire ou professionnel - des troupes passant du statut d'amateur à celui de professionnel et vice-versa, d'autres engageant des professionnel.le.s parfois sur une base régulière ou provisoire, d'autres enfin reposant sur un noyau stable d'agents rémunérés, etc.

Le colloque sera l'occasion d'un précieux et dialogue. Des praticien.ne.s, des théoricien.ne.s, des critiques, des historien.ne.s, des administrateurs et administratrices sont ainsi invités à échanger sur la question des théâtres des collectivités à partir de cinq axes de réflexion :

-   Les perspectives historiques, épistémologiques et théoriques

-   Les quêtes esthétiques, les pratiques innovantes

-   L'engagement militant : revendications sociales, identitaires, politiques; actions sur le milieu et pour le milieu

-   Les défis organisationnels, les modes de fonctionnement, les rapports avec les autres pratiques théâtrales et scéniques

-   Les pratiques de réception : comment voir et critiquer le théâtre des collectivités ?

[^1]: Par exemple les théâtres autochtones, ceux des minorités ethno-culturelles, le théâtre pour personnes vivant avec des déficiences, celui issu des communautés LBGTQ+, les troupes régionales et locales, le théâtre en milieu scolaire, le théâtre en institution (le théâtre dans les prisons, etc.) et en entreprise, etc.
