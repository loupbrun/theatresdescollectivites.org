---
title: Transportation
description: "Montréal is very well served by public transportation. The metro is very efficient."
---

## Transport from the airport

- **Taxi**: a ride from the P.-E. Trudeau airport to downtown Montreal costs approximately $40.00 CAD.
- **Bus**: the 747 shuttle bus connects passengers to the Lionel-Groulx station.
  It is possible from there to get to the University by metro.
  A ticket can be bought at the airport ($10.00 CAD).

## Transport in Montréal

It is possible to travel through the whole city of Montréal using public transport, either the bus or the metro.
Various ticket types are available to accomodate for all your needs.
For more information, please visit the site of the STM:
http://www.stm.info/en

