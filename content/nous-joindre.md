---
title: Nous joindre
description: "Pour toutes questions, n’hésitez pas à nous joindre."
---

**CRILCQ/Université de Montréal**  
Département des littératures de langue française  
Pavillon Lionel-Groulx, local C-8141  
C.P. 6128, succ. Centre-Ville  
Montréal (Québec) H3C-3J7  
Tél.: 514-343-6111, poste 7369

[theatredescollectivites@gmail.com](mailto:theatredescollectivites@gmail.com)
