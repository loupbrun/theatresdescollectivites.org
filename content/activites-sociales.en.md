---
title: Social Activities
description: "Various activities – expositions, book launches, shows – are planned during the colloquium."
---

**Cirque du Soleil – Kooza**  
13 May to August 14  
More info&nbsp;: https://www.cirquedusoleil.com/fr/kooza

**Festival Art Souterrain**  
April 2 to June 30  
More info : https://www.festival2022.artsouterrain.com

**Festival MURAL**  
June 9-19  
More info : https://www.lavitrine.com/evenement/Festival_MURAL

**Festival St-Ambroise Fringe de Montréal**  
May 30 to June 19  
More info : https://montrealfringe.ca/

**Francos de Montréal**  
June 10-18  
More info : https://francosmontreal.com

**Jardins Gamelin**  
May 26 mai to October 2  
More info : https://jardinsgamelin.com/fr/accueil
