---
title: Activités sociales
description: "Diverses activités – exposition, lancements de livres, spectacles – sont au programme du colloque."
---

**Cirque du Soleil – Kooza**  
Du 13 mai au 14 août  
Informations&nbsp;: https://www.cirquedusoleil.com/fr/kooza

**Festival Art Souterrain**  
Du 2 avril au 30 juin  
Informations : https://www.festival2022.artsouterrain.com

**Festival MURAL**  
Du 9 au 19 juin  
Informations : https://www.lavitrine.com/evenement/Festival_MURAL

**Festival St-Ambroise Fringe de Montréal**  
Du 30 mai au 19 juin  
Informations : https://montrealfringe.ca/

**Francos de Montréal**  
Du 10 au 18 juin  
Informations : https://francosmontreal.com

**Jardins Gamelin**  
Du 26 mai au 2 octobre  
Informations : https://jardinsgamelin.com/fr/accueil
