---
title: Lodging & Hotels
description: "The Montréal region offers numerous lodging possibilities for your stay during the colloquium. Here’s our suggested hotel list."

published: false

---

**Post-COVID information to come**

## Studios Hotel – Université de Montréal

SINGLE room: 50 CAD per night  
Suite: 110 CAD per night  

Contact: Organizing Committee

## Hostel Le jardin d’Antoine

2024, Saint-Denis Street  
Montreal, Quebec, H2X 2K8  
Phone number: 514-843-4506  
No additional fees: 1-800-361-4506  
Fax: 514-281-1491  
PRICES: 128 CAD per night (budget room) and   156 CAD (standard room)
Breakfast included

Website:
https://www.aubergelejardindantoine.com

Hotel contact:
reception@aubergelejardindantoine.com

*This hotel offers discount rates for University of Montreal conference participants.*

## Hotel Château Versailles

1659, Sherbrooke West Street  
Montreal, Quebec, H3H 1E3  
Phone number: 514-933-3611  
No additional fees: 888-933-8111  
Fax: 514-933-6967  
PRICES: 139 CAD (double room), 144 CAD/169 CAD (king size bed room)  
Breakfast included  

Website:
https://www.chateauversaillesmontreal.com

*This hotel offers discount rates for University of Montreal conference participants.*

## Terrasse Royale

5225, Côte-des-Neiges  
Montreal, Quebec, H3T 1Y1  
Phone number: 514-739-6391  
Fax: 514-342-2512  
PRICES: 129 CAD (studio king bed with small   kitchen)
Breakfast not included, but a discount   available for the hotel’s clients.

Website:
https://terrasse-royale.com  

Hotel 
ontact: info@terrasse-royale.com

*This hotel offers discount rates for University of Montreal conference participants.*

## Hostel Le Pomerol

819 boul. de Maisonneuve Est  
Montréal, Québec, H2L 1Y7  
Phone number: 514-526-5511  
No additional fees: 1-800-361-6869  
Fax: 514-523-0143  
PRICES: 155 CAD (double bedroom)  
Breakfast included  

Website:
https://www.aubergelepomerol.com  

Hotel contact:
info@aubergelepomerol.com

## Hotel de l’institut

3535 Saint-Denis Street  
Montreal, Quebec, H2X 3P1  
Phone number: 438-800-4595  
No additional fees: 1-855-229-8189 (Canada &   USA)
Fax: 514-873-9893  
PRICES: 229 CAD (regular room with king size   bed) + 20 CAD for an extra person
Breakfast included  

Website:
http://www.ithq.qc.ca/hotel  

Hotel contact:
hotel@ithq.qc.ca

## Hotel Le Relais Lyonnais

1595 Saint-Denis Street  
Montreal, (Quebec), H2X 3K3  
Phone number: 514-448-2999  
Fax: 514-448-2991  
PRICES: 199 CAD (double room with queen size   bed) and 259 CAD (suite)

Website:
http://lerelaislyonnais.com

## Residence Inn par Mariott (Montréal Centre-Ville)

2045 Peel Street  
Montreal, (Quebec), H3A 1T6  
Phone number: 514-935-9224  
PRICES: 278 CAD (studio with queen size bed) and 283 CAD (suite separate bedroom and living room)
Breakfast included  

Website:
https://www.marriott.fr/hotels/travel/yulri-residence-inn-montreal-downtown/

## Hotel Ambrose

3422, Stanley Street  
Montreal, Quebec, H3A 1R8  
Phone number: 514-288-6922  
Fax: 514-288-5757  
PRICES: 180 CAD (double room with queen size   bed)
Breakfast included for additional 10 CAD  

Website:
https://www.hotelambrose.ca/fr  

Hotel contact:
info@hotelambrose.ca

## Novotel Montréal Centre-Ville

1180 de la Montagne Street  
Montreal, Quebec, H3G 1Z1  
Phone number: 514-861-6000  
PRICES: From 219 CAD to 279 CAD (depending on  reservation date) (standard room with king size bed)
Breakfast included  
Parking: 30 CAD per night  

Website:
https://all.accor.com/hotel/1151/index.fr.shtml
