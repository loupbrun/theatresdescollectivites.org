.PHONY: build

help:
	@echo "Usage: make <command>"
	@echo "  serve  Runs a development webserver on port 1313"
	@echo "  build   Build the site with minification"
	@echo "  build-staging   Build the site with *staging* configuration"

serve:
	hugo serve --disableFastRender

build:
	rm -rf public
	hugo --minify 

build-staging:
	hugo --environment staging --minify
all-staging:
	make build-staging && ./deploy-staging.sh

all:
	make build && ./deploy.sh
