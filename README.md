# theatresdescollectivites.org

Site web pour le colloque international _Les théâtres des collectivités_.

## Installation

Ceci est un site qui se construit avec [Hugo](https://gohugo.io).
[Nodejs](https://nodejs.org) doit également être installé pour fabriquer les feuilles de style.

Naviguer dans le dossier theme/tailwind et installer le outils:

```bash
npm install
```

Installation terminée.

## Éditer/développer

Dans la racine du projet, lancer un serveur de développement

```bash
make serve      # serveur à l’adresse http://localhost:1313 par défaut
```

Les fichiers de contenu sont dans le dossier `content/`.

Les paramètres de configuration sont dans le fichier `config.yaml`.

## Production

```bash
make build       # les fichiers seront produits dans le dossier `public/`
```

Fichiers statiques à déployer sur un serveur.

Avec les accès au serveur:

```bash
make deploy
```
